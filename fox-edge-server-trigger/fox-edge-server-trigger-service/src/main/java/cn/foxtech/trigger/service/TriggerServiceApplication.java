package cn.foxtech.trigger.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TriggerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TriggerServiceApplication.class, args);
    }

}
